// Include Cron //
const { rejects } = require("assert");
const cron = require("cron");
const csv = require("csv-parser");
const fs = require("fs");
const { resolve } = require("path");

const cronJob = cron.CronJob;

let before, after;

new cronJob(
  "* * * * * 1",
  () => {
    console.log("Batch Running on : " + new Date());
    main();
  },
  null,
  true,
  ""
);

main = async () => {
  try {
    const result = await readFile("data.csv");
    // result.forEach((data) => {
    //   console.log(data);
    // });

    // RETRIEVE TOP 10 //
    let sortByCache = result.sort((a, b) => {
      return (
        b["CACH: cache memory in kilobytes"] -
        a["CACH: cache memory in kilobytes"]
      );
    });
    let topTen = sortByCache.slice(0, 10);
    console.log("------ TOP 10 HIGHEST CACHE MEMORY ---- ");
    console.log(topTen);

    // GET LOWEST MACHINE CYCLE //
    const minMachineCycle = Math.min.apply(
      Math,
      result.map((data) => {
        return data["MYCT: machine cycle time in nanoseconds"];
      })
    );
    const minMachineCycleData = result.find(
      (element) =>
        element["MYCT: machine cycle time in nanoseconds"] == minMachineCycle
    );
    console.log("------ SHORTEST MACHINE CYCLE TIME ---- ");

    console.log(" Min Machine Cycle Data : ", minMachineCycleData);

    //  * - list the next top 10 model name that have huge difference of relative performance between published and estimated - //
    let sortByGap = result.sort((a, b) => {
      let a_diff =
        a["PRP: published relative performance"] -
        a["ERP: estimated relative performance from the original article"];
      let b_diff =
        b["PRP: published relative performance"] -
        b["ERP: estimated relative performance from the original article"];

      return b_diff > a_diff;
    });
    let biggestGap = sortByGap.slice(sortByGap.length - 11, 10);
    console.log("------ HUGE DIFFERENCE OF RELATIVE PERFORMANCE ---- ");
    console.log(biggestGap);
  } catch (ex) {
    console.log(ex);
  }
};

// const maxCache = Math.max.apply(
//   Math,
//   data.map((data) => {
//     return data["CACH: cache memory in kilobytes"];
//   })
// );
// console.log("Top Model : ", maxCache);
// data.filter(())

function readFile(fileName) {
  console.log(" --- Reading File ----");
  before = process.hrtime();

  let arr = [];

  return new Promise((resolve, reject) => {
    fs.createReadStream(fileName)
      .pipe(csv())
      .on("data", (r) => {
        arr.push(r);
      })
      .on("error", (error) => {
        reject(error);
      })
      .on("end", () => {
        after = process.hrtime(after);
        console.log("Read File : ", after);
        resolve(arr);
      });
  });
}
